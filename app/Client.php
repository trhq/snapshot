<?php

namespace Snapshot;

use Illuminate\Database\Eloquent\Model;
// Bootsttrapper Classes
use Bootstrapper\Facades\Accordion as Accordion;
// Sluggable
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use Snapshot\SnapshotTraits as SnapshotTraits;

/**
 * Class Client
 * This Model defines a client who requests photography work (projects).
 * @package snapshot
 */
class Client extends Model implements SluggableInterface {
    use SnapshotTraits;
    use SluggableTrait;


    /**
     * Converts the Client name to a URL slug.
     * (http://url.com/client-name)
     * @var array
     */
    protected $sluggable = array(
        "build_from" => "name",
        "save_to" => "slug"
    );

    /**
     *  Defines a one to many relationship with Projects.
     * (one Client requests many Projects)
     */
    public function projects()
    {
        return $this->hasMany('Snapshot\Project');
    }

    /**
     *  Defines a one to many relationship with Projects.
     * (one Client requests many Projects)
     */
    public function shoots()
    {
        return $this->belongsToMany('Snapshot\Shoot');
    }

    /**
     * Defines a one to one relationship with Comments.
     * (one Client has a single Comment)
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function comments()
    {
        return $this->morphOne('\Snapshot\Comment', 'commentable');
    }
}
