<?php

namespace Snapshot;

use Illuminate\Database\Eloquent\Model;

use Snapshot\SnapshotTraits as SnapshotTraits;

class Comment extends Model
{
    use SnapshotTraits;

    public function commentable() {
      return $this->morphTo();
    }
}
