<?php

namespace Snapshot\Http\Controllers;

use Illuminate\Http\Request;

use Snapshot\Http\Requests;
use Snapshot\Http\Controllers\Controller;
use Snapshot\Http\Requests\ClientFormRequest;

use Snapshot\Client as Client;
use Snapshot\Comment as Comment;
use Snapshot\Project as Project;
use Snapshot\Shoot as Shoot;
use Snapshot\SnapshotTraits as SnapshotTraits;

class ClientController extends Controller {
    use SnapshotTraits;

    protected $imgPath = "/home/dev/Sites/snapshot.app/resources/assets/images/";

    public function index()
    {
        $clients = Client::all();

        return view('client.index')->with('clients', $clients);
    }

    public function show($id)
    {
        $client = Client::findBySlug($id);

        return view('client.show')->with('client', $client);
    }

    public function create()
    {
        return view('client.create');
    }

    public function edit($id)
    {
        $client = Client::findBySlug($id);

        return view('client.edit')->with('client', $client);
    }

    public function store(ClientFormRequest $request)
    {
        // Create a new Client object
        $client = new Client;
        $client = $this->makeClient($client, $request);
        $client->save();
        // If there's a client comment, add it to the comments table.
        $this->saveComment($client, $request);
        // Create a project if one was included.
        
        // Make a new image directory for the client.
        mkdir($this->imgPath.$client->id);

        $project = $this->createClientProject($client, $request);
        if ($project) $shoot = $this->createProjectShoot($client, $project, $request);

        // Redirect to the newly created client's page.
        return \Redirect::route('client.show', array('slug' => $client->slug))->with('message', "$client->name was created. ");
    }

    public function update($id, ClientFormRequest $request)
    {
        // Get the client from the database and update fields.
        $client = Client::findBySlug($id);
        $client = $this->makeClient($client, $request);
        // Update the client
        $client->update();
        // If there's a submitted comment, update or save it it in the comments table.
        $this->updateComment($client, $request);

        // Redirect to the newly created clients page.
        return \Redirect::route('client.show', array('slug' => $client->slug))->with('message', "$client->name was updated. ");
    }

    public function destroy($id)
    {
        // Get the client from the database to create the message.
        $client = Client::find($id);
        $message = $client->name . " was deleted.";
        $this->deleteDirectory($this->imgPath.$client->id);
        
        // Delete the cient
        Client::destroy($id);

        // Redirect to the client index.
        return \Redirect::route('client.index')->with('message', $message);
    }

    public function createClientProject(Client $client, ClientFormRequest $request) {
        $project = null;
        if ($request->get('projectName'))
        {
            $project = new Project;
            $project->name = $request->get('projectName');
            $client->projects()->save($project);
            mkdir($this->imgPath.$client->id."/".$project->id);
        }

        return $project;
    }

    public function createProjectShoot(Client $client, Project $project, ClientFormRequest $request) {
        // Create a shoot if one was included.
        if ($request->get('shootName') && isset($project))
        {
            $shoot = new Shoot;
            $shoot->name = $request->get('shootName');
            $shoot->user_id = 1;
            $project->shoots()->save($shoot);
            mkdir($this->imgPath.$client->id."/".$project->id."/".$shoot->id);
        }

        return $shoot;
    }

}
