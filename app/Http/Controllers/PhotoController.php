<?php

namespace Snapshot\Http\Controllers;

use Illuminate\Http\Request;

use Snapshot\Http\Requests;
use Snapshot\Http\Controllers\Controller;
// use Snapshot\Http\Requests\PhotoFormRequest;

use Snapshot\Photo as Photo;
use Snapshot\Shoot as Shoot;
use Snapshot\Project as Project;
use Snapshot\Client as Client;
use Snapshot\Comment as Comment;

class PhotoController extends Controller {
    public function index()
    {
        // $photos = Photo::all();
        return view('photo.index');
    }

    public function show($id)
    {
        // $photo = Photo::findBySlug($id);
        // return view('photo.show')->with('photo', $photo);
    }

    public function create()
    {
        // return view('photo.create');
    }

    public function edit($id)
    {
        // $photo = Photo::findBySlug($id);
        // return view('photo.edit')->with('photo',$photo);
    }

    public function store(PhotoFormRequest $request)
    {
        // // Create a new Client object
        // $photo = new Client;
        // $photo = $this->makeClient($photo, $request);
        // $photo->save();
        // // If there's a photo comment, add it to the comments table.
        // $this->saveComment($photo, $request);
        // Redirect to the newly created photos page.
        return \Redirect::route('photo.show', array('slug' => $photo->slug))->with('message', "$photo->name was created. ");
    }

    public function update($id, PhotoFormRequest $request)
    {
        // // Get the photo from the database and update fields.
        // $photo = Photo::findBySlug($id);
        // $photo = $this->makeClient($photo, $request);
        // // Update the photo
        // $photo->update();
        // // If there's a submitted comment, update or save it it in the comments table.
        // $this->updateComment($photo, $request);
        //

        // Redirect to the newly created photos page.
        return \Redirect::route('photo.show', array('slug' => $photo->slug))->with('message', "$photo->name was updated. ");
    }

    public function destroy($id)
    {
        // // Get the photo from the database to create the message.
        // $photo = Photo::find($id);
        // $message = $photo->name . " was deleted.";
        // // Delete the cient
        // Photo::destroy($id);
        // // Redirect to the photo index.
        // return \Redirect::route('photo.index')->with('message', $message);
    }

    /**
     * Takes a photo and request and populates $photos fields.
     * @method makeClient
     *
     * @param  Client           $photo   A Client object.
     * @param  PhotoFormRequest $request A PhotoFormRequest object
     *
     * @return snapshot/Client            A Client object.
     */
    protected function makeClient(Client $photo, PhotoFormRequest $request)
    {
        $photo->name = $request->get('name');
        $photo->company = $request->get('company');
        $photo->email = $request->get('email');
        $photo->phone = $request->get('phone');

        return $photo;
    }

    protected function saveComment(Client $photo, PhotoFormRequest $request)
    {
        // If there's a photo comment, add it to the comments table.
        if ($request->get('comment'))
        {
            $comment = new Comment;
            $comment->comment = $request->get('comment');
            $photo->comments()->save($comment);
        }
    }

    protected function updateComment(Client $photo, PhotoFormRequest $request)
    {
        // If there is no existing comment, call saveComment() instead.
        if (!isset($photo->comments->id))
        {
            $this->saveComment($photo, $request);

            return;
        }
        // Otherwise, get the existing comment from the database.
        $comment = Comment::find($photo->comments->id);
        // If comment is non-empty, update it.
        if ($request->get('comment'))
        {
            $comment->comment = $request->get('comment');
            $comment->update();

            return;
            // Otherwise, delete it.
        } else
        {
            Comment::destroy($comment->id);

            return;
        }
    }
}
