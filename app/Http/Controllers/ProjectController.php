<?php

namespace Snapshot\Http\Controllers;

use Snapshot\Http\Requests\ProjectFormRequest;

use Illuminate\Http\Request;

use Snapshot\Http\Requests;
use Snapshot\Http\Controllers\Controller;

use Snapshot\Project as Project;
use Snapshot\Client as Client;
use Snapshot\Comment as Comment;
use Snapshot\SnapshotTraits as SnapshotTraits;

class ProjectController extends Controller {
    use SnapshotTraits;

    public function index(Request $request)
    {
        $client_id = $request->input('client_id');
        if ($client_id) {
            $projects = Project::with('client', 'shoots')->where('client_id', $client_id)->get();
        } else {
            $projects = Project::with('client', 'shoots')->get();
        }
        return view('project.index')->with('projects', $projects);
    }

    public function show($slug)
    {
        // $slugged = Project::findBySlug($slug);
        $project = Project::with('client', 'shoots')->where('slug', $slug)->get();
        $project = $project[0];

        return view('project.show')->with('project', $project);
    }

    public function create()
    {
        $clients = Client::all();

        return view('project.create')->with('clients', $clients);
    }

    public function edit($slug)
    {
        $project = Project::findBySlug($slug);

        return view('project.edit')->with('project', $project);
    }

    public function store(ProjectFormRequest $request)
    {
        // Create a new Project object
        $project = new Project;
        $project = $this->makeProject($project, $request);
        $project->save();
        // If there's a project comment, add it to the comments table.
        $this->saveComment($project, $request);

        // Redirect to the newly created projects page.
        return \Redirect::route('project.show', array('slug' => $project->slug))->with('message', "$project->name was created. ");
    }

    public function update($id, ProjectFormRequest $request)
    {
        // // Get the project from the database and update fields.
        $project = Project::findBySlug($id);
        $project = $this->makeProject($project, $request);
        // // Update the project
        $project->update();
        // // If there's a submitted comment, update or save it it in the comments table.
        $this->updateComment($project, $request);

        // Redirect to the newly created projects page.
        return \Redirect::route('project.show', array('slug' => $project->slug))->with('message', "$project->name was updated. ");
    }

    public function destroy($id)
    {
        // // Get the project from the database to create the message.
        $project = Project::find($id);
        $message = $project->name . " was deleted.";
        // // Delete the cient
        Project::destroy($id);

        // // Redirect to the project index.
        return \Redirect::route('project.index')->with('message', $message);
    }
}
