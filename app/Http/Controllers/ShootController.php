<?php

namespace Snapshot\Http\Controllers;

use Illuminate\Http\Request;

use Snapshot\Http\Requests;
use Snapshot\Http\Controllers\Controller;
use Snapshot\Http\Requests\ShootFormRequest;

use Snapshot\Shoot as Shoot;
use Snapshot\Project as Project;
use Snapshot\Client as Client;
use Snapshot\Comment as Comment;
use Snapshot\SnapshotTraits as SnapshotTraits;

class ShootController extends Controller {
    use SnapshotTraits;

    public function index(Request $request)
    {
        // Check if client or project IDs were passed with the request.
        $client_id = $request->input('client_id');
        $project_id = $request->input('project_id');

        // Create an empty $client variable for views.
        $client ="";

        // If a project ID is passed, only get its shoots
        if ($project_id) {
            $shoots = Shoot::where('project_id', $project_id)->get();
        // If a client ID is passed, only get their shoots.
        } else if ($client_id) { 
        	$client = Client::find($client_id);
        	$shoots = $client->shoots;
        } else {
        // Otherwise, get all shoots.
            $shoots = Shoot::with('project', 'clients')->get();
        }
        
        return view('shoot.index')->with('shoots', $shoots)->with('client', $client);
    }

    public function show($slug)
    {
        // $shoot = Shoot::findBySlug($slug);
        $shoot = Shoot::with('project', 'photos')->where('slug', $slug)->first();

        return view('shoot.show')->with('shoot', $shoot);
    }

    public function create()
    {
        return view('shoot.create');
    }

    public function edit($id)
    {
        $shoot = Shoot::findBySlug($id);

        return view('shoot.edit')->with('shoot', $shoot);
    }

    public function store(ShootFormRequest $request)
    {

    	$shoot = $this->createShoot($request);

        // Redirect to the newly created shoots page.
        return \Redirect::route('shoot.show', array('slug' => $shoot->slug))->with('message', "$shoot->name was created. ");
    }

    public function update($id, ShootFormRequest $request)
    {

    	$shoot = $this->updateShoot($id, $request);

        // Redirect to the updated shoots page.
        return \Redirect::route('shoot.show', array('slug' => $shoot->slug))->with('message', "$shoot->name was updated. ");
    }

    public function destroy($id)
    {
        // // Get the shoot from the database to create the message.
        $shoot = Shoot::find($id);
        $message = $shoot->name . " was deleted.";
        // // Delete the cient
        Shoot::destroy($id);

        // // Redirect to the shoot index.
        return \Redirect::route('shoot.index')->with('message', $message);
    }

    /**
     * Takes a ShootFormRequest and creates a new Shoot.
     * @param  ShootFormRequest $request 	A FormRequest object.
     * @return Snapshot\Shoot               
     */
    protected function createShoot(ShootFormRequest $request) {
    	        // Create a new Shoot object
        $shoot = new Shoot;
        $shoot = $this->makeShoot($shoot, $request);
        $shoot->save();
        // Link to client_id
        $this->syncClient($shoot, [$shoot->project->client_id]);

        // // If there's a shoot comment, add it to the comments table.
        $this->saveComment($shoot, $request);

        return $shoot;
    }

    protected function updateShoot($id, ShootFormRequest $request) {
        // // Get the shoot from the database and update fields.
        $shoot = Shoot::findBySlug($id);
        $shoot = $this->makeShoot($shoot, $request);
        // Update the shoot
        $shoot->update();
        // Update the client_id 
        $this->syncClient($shoot, [$shoot->project->client_id]);
        // If there's a submitted comment, update or save it it in the comments table.
        $this->updateComment($shoot, $request);

    	return $shoot;
    }

    protected function syncClient(Shoot $shoot, array $client) {
    	$shoot->clients()->sync($client);
    }

}
