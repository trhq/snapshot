<?php

namespace Snapshot\Http\Requests;

use Snapshot\Http\Requests\Request;
use Snapshot\Client as Client;

class ClientFormRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $c = new Client;
        $c = $c->findBySlug($this->client);
        // dd($c);
        // dd($c->id);
        switch ($this->method())
        {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'name'  => 'required',
                    'email' => 'required|email|unique:clients,email',
                    'phone' => 'required',
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'name'  => 'required',
                    'email' => 'required|email|unique:clients,email,' . $c->id,
                    'phone' => 'required',
                ];
        }
    }
}
