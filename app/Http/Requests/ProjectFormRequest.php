<?php

namespace Snapshot\Http\Requests;

use Snapshot\Http\Requests\Request;
use Snapshot\Client as Client;
use Snapshot\Project as Project;

class ProjectFormRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $p = new Project;
        // $p = $p->findBySlug($this->project);
        // dd($c);
        // dd($c->id);
        switch ($this->method())
        {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'client_id' => 'required',
                    'name'      => 'required',
                ];

            case 'PUT':
            case 'PATCH':
                return [
                ];
        }
    }
}
