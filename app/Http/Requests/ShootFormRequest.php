<?php

namespace Snapshot\Http\Requests;

use Snapshot\Http\Requests\Request;
use Snapshot\Client as Client;
use Snapshot\Project as Project;
use Snapshot\Shoot as Shoot;

class ShootFormRequest extends Request {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $c = new Shoot;
        $c = $c->findBySlug($this->shoot);
        // dd($c);
        // dd($c->id);
        switch ($this->method())
        {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'project_id' => 'required',
                    'name'       => 'required',
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'project_id' => 'required',
                    'name'       => 'required',
                ];
        }
    }
}
