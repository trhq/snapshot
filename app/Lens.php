<?php

namespace Snapshot;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Lens
 * This Model defines a Lens which is used to take a photograph.
 *
 * @package snapshot
 */
class Lens extends Model {

    /**
     * Defines one to many relationship with Photo.
     * (one Camera takes many Photos)
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function photos()
    {
        return $this->hasMany('Snapshot\Photo');
    }

    /**
     * Defines a many to one relationship with Maker.
     * (many Cameras are made by one Maker)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function maker()
    {
        return $this->belongsTo('Snapshot\Maker');
    }
}
