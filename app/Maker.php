<?php

namespace Snapshot;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Maker
 * This Model defines a manufacturer who makes Cameras and/or Lenses
 *
 * @package snapshot
 */
class Maker extends Model {

    /**
     * Defines a one to many relationship with Camera bodies.
     * (one Maker makes many Cameras)
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cameras()
    {
        return $this->hasMany('Snapshot\Camera');
    }

    /**
     * Defines a one to many relationship with Lenses.
     * (one Maker makes many Lenses)
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lens()
    {
        return $this->hasMany('Snapshot\Lens');
    }
}
