<?php
namespace Snapshot;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Photo
 * This Model defines a photograph which was captured as part of a shoot.
 * It is responsible for adding, retrieving and removing photos from
 * the database AND the server's filesystem.
 *
 * @package snapshot
 */
class Photo extends Model {

    /**
     * Defines a many to one relationship with Shoots.
     * (many Photos are taken during one Shoot)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function shoot()
    {
        return $this->belongsTo('Snapshot\Shoot');
    }

    /**
     * Defines a many to one relationship with camera.
     * (many Photos are taken by one Camera)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function camera()
    {
        return $this->belongsTo('Snapshot\Camera');
    }

    /**
     * Defines a many to one relationship with Lens.
     * (many Photos are taken with one Lens)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lens()
    {
        return $this->belongsTo('Snapshot\Lens');
    }
}
