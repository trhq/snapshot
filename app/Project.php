<?php

namespace Snapshot;

use Illuminate\Database\Eloquent\Model;
// Bootsttrapper Classes
use Bootstrapper\Facades\Accordion as Accordion;
// Sluggable
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use Snapshot\SnapshotTraits as SnapshotTraits;

/**
 * Class Project
 * This Model defines a Project which contains many Shoots.
 * Projects belong to Clients.
 *
 * @package snapshot
 */
class Project extends Model implements SluggableInterface {
    use SnapshotTraits;
    use SluggableTrait;

    /** @var array Model Attributes which should be treated as dates */
    protected $dates = ['deadline'];

    /**
     * Converts the Project name to a URL slug.
     * (http://url.com/project-name)
     *
     * @var array
     */
    protected $sluggable = array(
        "build_from" => "name",
        "save_to"    => "slug",
    );

    /**
     * Defines a one to many relationship between Client and Project.
     * (many Projects are carried out for one Client)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo('Snapshot\Client');
    }

    /**
     * Defines a one to many relationship with Shoots.
     * (one Project has many Shoots)
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function shoots()
    {
        return $this->hasMany('Snapshot\Shoot');
    }

    /**
     * Defines a one to one relationship for comments.
     * (one Project has a single Comment)
     * @method comments
     *
     * @return Snapshot\Comment   This project's comment.
     */
    public function comments()
    {
        return $this->morphOne('\Snapshot\Comment', 'commentable');
    }
}
