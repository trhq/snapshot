<?php

namespace Snapshot\Providers;

use Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Format a date
        Blade::directive('date', function($date) {
            return "<?php echo with{$date}->format('j/m/Y'); ?>";
        });
    }


    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    	if ($this->app->environment() == 'local') {
    		$this->app->register('Laracasts\Generators\GeneratorsServiceProvider');
	   }
    }
}
