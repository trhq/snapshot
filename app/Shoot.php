<?php

namespace Snapshot;

use Illuminate\Database\Eloquent\Model;

// Bootsttrapper Classes
// Sluggable
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;


use Snapshot\Shoot as Shoot;
use Snapshot\Project as Project;
use Snapshot\Client as Client;
use Snapshot\Comment as Comment;
use Snapshot\SnapshotTraits as SnapshotTraits;

/**
 * Class Shoot
 * This Model defines a photo shoot where a photographer takes many
 * photos. A photo shoot belongs to a project which is for a client.
 *
 * @package snapshot
 */
class Shoot extends Model implements SluggableInterface {
    use SnapshotTraits;
    use SluggableTrait;

    /**
     * Converts the Shoot name to a URL slug.
     * (http://url.com/shoot-name)
     *
     * @var array
     */
    protected $sluggable = array(
        "build_from" => "name",
        "save_to"    => "slug",
    );

    protected $dates = ['date'];


    /**
     * Defines a one to many relationship to photos.
     * (one Shoot contains many Photos)
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function photos()
    {
        return $this->hasMany('Snapshot\Photo');
    }

    /**
     * Defines a many to one relationship with Users.
     * (many Shoots are undertaken by one User)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('Snapshot\User');
    }

    /**
     * Defines a many to one relationship with Projects.
     * (many Shoots are part of one Project)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo('Snapshot\Project');
    }

    /**
     * Defines a many to one relationship with Projects.
     * (many Shoots are part of one Project)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function clients()
    {
        return $this->belongsToMany('Snapshot\Client');
    }

    /**
     * Defines a one to one relationship with Comments.
     * (one shoot has a single Comment)
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function comments()
    {
        return $this->morphOne('\Snapshot\Comment', 'commentable');
    }
}
