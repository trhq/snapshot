<?php
namespace Snapshot;


use Illuminate\Database\Eloquent\Model;

// Sluggable
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use Snapshot\Http\Requests\ClientFormRequest;
use Snapshot\Http\Requests\ProjectFormRequest;
use Snapshot\Http\Requests\ShootFormRequest;


trait SnapshotTraits {

    /**
     * Gets a list of projects for project dropdowns.
     * @method projectList
     *
     * @param  Boolean $placeholder True if you want to add a null placeholder at the start of the $list array.
     *
     * @return array $list                An array: $list[$id] => $displayName()
     */
    public static function projectList()
    {
        $projects = Project::all();
        $list = array();
        // Add a placeholder
        $list[null] = "Select a project...";
        foreach ($projects as $project)
        {
            $list[$project->id] = $project->displayName();
        }

        return $list;
    }

    /**
     * Gets a list of clients for shoot dropdowns.
     * @method clientList
     *
     * @param  Boolean $placeholder True if you want to add a null placeholder at the start of the $list array.
     *
     * @return array $list                An array: $list[$id] => $displayName()
     */
    public static function clientList($placeholder = false)
    {
        $clients = Client::all();
        $list = array();
        if ($placeholder) $list[null] = "Select a client...";
        foreach ($clients as $client)
        {
            $list[$client->id] = $client->displayName();
        }

        return $list;
    }

    public function displayName($link = false)
    {
        $route = $this->getRoute();
        $text = $this->name;
        if ($this->name && $this->company) $text .= " - ";
        $text .= $this->company;
        if ($link) $text = \HTML::link(\URL::route($route . '.show', ['slug' => $this->slug]), $text, ['title' => "View $text"]);

        // }
        return $text;
    }

    public function displayDate() {
        // Return the formatted date if there is only one field.
        if ( count($this->dates == 1) ) {
            $date = $this->dates[0];
            if ($this->$date) return $this->$date->format('j/m/Y');
        }
    }

    public function getRoute()
    {
        $route = get_class($this);
        $route = str_replace('Snapshot\\', '', $route);
        $route = strtolower($route);

        return $route;
    }

    /**
     * Returns a list array of table rows for a table
     * that lists projects.
     * @method project_row
     *
     * @return array     An array of table rows.
     */

    public function project_row()
    {
        $project = $this;

        $btn["edit"] = $this->btn_edit('project');
        $btn["view"] = $this->btn_view('project');

        $row = [
            "<strong>Client</strong>"   => \HTML::link(\URL::route('client.show', ['slug' => $project->client->slug]), $project->client->displayName()),
            "<strong>Project</strong>"  => $project->name,
            "<strong>Shoots</strong>"   => $project->shoots->count(),
            "<strong>Deadline</strong>" => $project->deadline,
            "<strong>Actions</strong>"  => $project->btn_view('project') . ' ' . $project->btn_edit('project'),
        ];

        return $row;
    }


    /**
     * Returns a view button for this object which links to the show route defined for the supplied $route controller.
     * @method btn_view
     *
     * @param  String         $route  The name of the top-level route controller for this model.
     * @param  Snapshot\Model $object Explicitly pass a model to view.
     *
     * @return Html                         Bootstrap HTML View button.
     */

    public function btn_view()
    {
        $route = $this->getRoute();

        return \Button::primary(\Icon::search())->asLinkTo(\URL::route($route . '.show', ["slug" => $this->slug]))->withAttributes(["title" => "View " . $this->displayName()]);
    }


    /**
     * Returns an edit button for this object which links to the edit route defined for the supplied $route controller.
     * @method btn_view
     *
     * @param  String         $route  The name of the top-level route controller for this model.
     * @param  Snapshot\Model $object Explicitly pass a model to view.
     *
     * @return Html                         Bootstrap HTML View button.
     */

    public function btn_edit()
    {
        $route = $this->getRoute();

        return \Button::withValue(\Icon::pencil())->asLinkTo(\URL::route($route . '.edit', ["slug" => $this->slug]))->withAttributes(["title" => "Edit " . $this->displayName()]);
    }


    /**
     * Returns a delete button for this object which presents a modal popup warning, then links to $routes destroy
     * controller method.
     * @method btn_delte
     *
     * @param  String         $route  The name of the top-level route controller for this model.
     * @param  Snapshot\Model $object Explicitly pass a model to view.
     *
     * @return Html                           Bootstrap HTML View button.
     */

    public function btn_delete()
    {
        $route = $this->getRoute();
        // Compile the Delete Warning message.
        $body = "<p>Are you sure you want to delete <em>{$this->displayName()}</em>? </p><p>If you proceed:</p> <ul>";
        switch ($route)
        {
            case 'client' && (!'project' && !'shoot'):
                $body .= "<li>All projects associated with $this->name will be permanently removed.</li>";
            case ('project' || 'client') && !'shoot':
                $body .= "<li>All shoots associated with $this->name will be permanently removed.</li>";
            case 'shoot' || 'project' || 'client':
                $body .= "<li>All photos associated with $this->name will be permanently removed.</li>";
                break;
            default:
                $body .= "<li> If you proceed $this->name will be permanently removed.</li>";
                break;
        }
        $body .= "</ul>";
        // Create the delete form button.
        $delForm = \Form::open(array('route' => array($route . '.destroy', $this->id), 'method' => 'delete'));
        $delForm .= \Button::danger(\Icon::ok() . " Delete")->withAttributes(["class" => "col-xs-5 col-xs-push-2"])->submit();
        $delForm .= \Form::close();

        return \Modal::named('delete-' . $this->id)
            ->withButton(\Button::danger(\Icon::trash())->withAttributes(["title" => "Delete $this->name"]))
            ->withTitle(\Icon::exclamation_sign() . ' Warning', 'alert-danger')
            ->withBody($body)
            ->withFooter(\Button::success(\Icon::remove() . " Cancel")->withAttributes(["data-dismiss" => "modal", "class" => 'col-xs-5']) . $delForm);

    }


    /**
     * Gets the projects for this Model, compiles them into an array of $titles and
     * $contents which includes the ouput from $project->shoots_accordion(), then calls
     * the makeAccordion() method and returns its output.
     * @method projects_accordion
     *
     * @return Html                     Bootstrapped Accordion Html
     */

    public function projects_accordion()
    {
        // Set a name for the accordion
        $name = "projects-$this->id";
        // And array for contents.
        $contents = array();
        // Add each project title and contents to the array
        foreach ($this->projects as $project)
        {
            // Compile the title for this project.
            $title = '<span class="col-xs-12 col-sm-10">' . $project->name . '</span>';
            $title .= '<small class="col-xs-6 col-sm-2">' . $project->text_shoot_count() . "</small>";
            $title = '<span class="container">' . $title . "</span>";
            // // Compile the contents for this project.
            $project_contents = '';
            $project_contents .= '<p class="col-xs-10">' . $project->getComments() . '</p>';
            $project_contents .= '<p class="col-xs-2">' . $project->btn_view() . '</p>';

            // // Add due date if it exists
            if (isset($project->deadline) && $project->deadline->year !== -1) $project_contents .= "<p class=\"col-xs-12\"> <strong> Due: </strong> {$project->deadline->format('l, j/m/Y')} </p>";
            // // Add comments if they exist.
            $project_contents .= '<h3 class="cols-xs-12">Shoots</h3>';
            $project_contents .= $project->shoots_accordion();

            $contents[] = ["title" => $title, "contents" => $project_contents];
        }

        return $this->makeAccordion($name, $contents);
    }


    /**
     * Gets the shoots for this Model, compiles them into an array of $titles and
     * $contents, then calls the makeAccordion() method and returns its output.
     * @method shoots_accordion
     *
     * @return Html                     Bootstrapped Accordion Html
     */

    public function shoots_accordion()
    {
        $name = 'shoots-' . $this->id;
        $contents = array();
        foreach ($this->shoots as $shoot)
        {
            $title = "<span class=\"container\"> <span class=\"col-xs-12 col-sm-10\">$shoot->name</span> <small class=\"col-xs-12 col-sm-2\">" . $shoot->text_photo_count() . "</small> </span>";
            $shoot_contents = $shoot->getComments();
            $contents[] = ["title" => $title, "contents" => $shoot_contents];
        }

        return $this->makeAccordion($name, $contents);
    }


    /**
     * Calls Bootstrapper/Accordion with the supplied $name and $contents.
     * @method makeAccordion
     *
     * @param  String $name     A unique name / id for the accordion.
     * @param  array  $contents An array of $titles and $contents for each accordion entry.
     *
     * @return Html                     Bootstrapped Accordion Html
     */

    public function makeAccordion($name, $contents)
    {
        return \Accordion::named($name)->withContents($contents);
    }


    /**
     * Takes a client and request and populates $clients fields.
     * @method makeClient
     *
     * @param  Client            $client  A Client object.
     * @param  ClientFormRequest $request A ClientFormRequest object
     *
     * @return snapshot/Client            A Client object.
     */

    protected function makeClient(Client $client, ClientFormRequest $request)
    {
        $client->name = $request->get('name');
        $client->company = $request->get('company');
        $client->email = $request->get('email');
        $client->phone = $request->get('phone');

        return $client;
    }


    /**
     * Takes a project and request and populates $projects fields.
     * @method makeClient
     *
     * @param  Project            $project A Project object.
     * @param  ProjectFormRequest $request A ProjectFormRequest object
     *
     * @return snapshot/Project            A Project object.
     */

    protected function makeProject(Project $project, ProjectFormRequest $request)
    {
        $input = $request->all();
        $project->name = $input['name'];
        $project->client_id = $input['client_id'];
        $project->deadline = $this->makeDate($input["deadline"]);

        return $project;
    }


    /**
     * Takes a shoot and request and populates $shoots fields.
     * @method makeShoot
     *
     * @param  Shoot            $shoot   A Shoot object.
     * @param  shootFormRequest $request A shootFormRequest object
     *
     * @return snapshot/Client            A Client object.
     */

    protected function makeShoot(shoot $shoot, shootFormRequest $request)
    {
        $input = $request->all();
        $shoot->name = $input['name'];
        $shoot->project_id = $input['project_id'];
        $shoot->user_id = 1;
        $shoot->date = $this->makeDate($input['date']);
        $shoot->location = $input['location'];

        return $shoot;
    }

    /**
     * Takes a dateString and returns a correctly formated date object
     * or null.
     * @method  makeDate
     *
     * @param  String $dateString
     *
     * @return Date Object or null
     */
    protected function makeDate($dateString)
    {
        $date = strtotime($dateString);
        if ($date)
        {
            $date = date('Y-m-d', $date);
        } else
        {
            $date = null;
        }

        return $date;
    }

    /**
     * Gets the comments for $this object.
     * @method getComments
     *
     * @return String      $this->comments->comment
     */

    public function getComments()
    {
        if ($this->comments) return $this->comments->comment;

    }


    /**
     * Creates a new comment object and saves it via the supplied $model.
     * @method saveComment
     *
     * @param  Laravel Model Object      $model  Can be any Eloquent Model which is commentable [Client, Project, Shoot]
     * @param  Laravel Form Request      $request Can be any Http Request object.
     *
     * @return Boolean                   True if a comment was saved. False if there's no comment.
     */

    protected function saveComment($model, $request)
    {
        // If there's a project comment, add it to the comments table and return true.
        if ($request->get('comment'))
        {
            $comment = new Comment;
            $comment->comment = $request->get('comment');
            $model->comments()->save($comment);

            return true;
        }

        // Return false if there was no comment.
        return false;
    }


    /**
     * Updates or deletes an existing comment, if one exists, or saves a new one.
     * @method updateComment
     *
     * @param  Laravel Model Object   $model  Can be any Eloquent Model which is commentable [Client, Project, Shoot]
     * @param  Laravel Form Request   $request Can be any Http Request object.
     *
     * @return Boolean                True if a comment was saved or updated. False if there's no comment or an
     *                                existing comment was deleted.
     */

    protected function updateComment($model, $request)
    {
        // If there is no existing comment, call saveComment() instead.
        if (!isset($model->comments->id))
        {
            $this->saveComment($model, $request);

            return;
        }
        // Otherwise, get the existing comment from the database.
        $comment = Comment::find($model->comments->id);
        // If a none empty comment is submitted, update it.
        if ($request->get('comment'))
        {
            $comment->comment = $request->get('comment');
            $comment->update();

            return;
            // Otherwise, if no comment is submitted, delete existing.
        } else
        {
            Comment::destroy($comment->id);

            return;
        }
    }


    /**
     * Gets the number of projects and returns it with appropriate text.
     * @method text_project_count
     *
     * @return string           A project count with the appropriate grammar.
     */

    public function text_project_count()
    {
        // Get the number of photo shoots for this project.
        $text_count = $this->projects->count();
        // Test for correct noun grammar
        if ($text_count == 1)
        {
            // Singular
            $text_count = $text_count . " project";
        } else
        {
            // Plural
            $text_count = $text_count . " projects";
        }

        return $text_count;
    }


    /**
     * Gets the number of shoots and returns it with appropriate text.
     * @method text_shoot_count
     *
     * @return string           A shoot count with the appropriate grammar.
     */

    public function text_shoot_count()
    {
        // Get the number of photo shoots for this project.
        $text_count = $this->shoots->count();
        // Test for correct noun grammar
        if ($text_count == 1)
        {
            // Singular
            $text_count = $text_count . " shoot";
        } else
        {
            // Plural
            $text_count = $text_count . " shoots";
        }

        return $text_count;
    }


    /**
     * Gets the number of photos and returns it with appropriate text.
     * @method text_photo_count
     *
     * @return string           A photo count with the appropriate grammar.
     */

    public function text_photo_count()
    {
        $count = 0;
        // Switch through and add the photos together depending on the model.
        switch ($this->getRoute())
        {
            case 'project':
                foreach ($this->shoots as $shoot)
                {
                    $count += $shoot->photos->count();
                }
                break;
            case 'shoot':
                $count = $this->photos->count();
                break;
        }
        // Get the number of photo photos for this project.
        $text_count = $count;
        // Test for correct noun grammar
        if ($text_count == 1)
        {
            // Singular
            $text_count = $text_count . " photo";
        } else
        {
            // Plural
            $text_count = $text_count . " photos";
        }

        return $text_count;
    }
    /**
     * Runs 'rm -rf $dir' to completely remove a directory
     * @param  string $dir  The full path to remove. 
     * @return inte         0, if successsful.
     */
    function deleteDirectory($dir) {
        system('rm -rf ' . escapeshellarg($dir), $retval);
        return $retval == 0; // UNIX commands return zero on success
    }

} // end Class
