<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shoot_id')->unsigned();
            $table->integer('camera_id')->unsigned();
            $table->integer('lens_id')->unsigned();
            $table->string('file_name', 255);
            $table->timestamp('capture_time')->nullable();
            $table->tinyInteger('rating')->default(0);
            $table->boolean('drop')->default(false);
            $table->timestamps();

        });
    }




    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('photos');
    }
}
