<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShootsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shoots', function (Blueprint $table) {
              $table->increments('id');
              $table->integer('user_id')->unsigned();
              $table->integer('project_id')->unsigned();
              $table->string('name', 75);
              $table->date('date')->nullable();
              $table->string('location', 255)->nullable();
              $table->text('comments')->nullable();
              $table->boolean('is_current')->default(false);
              $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shoots');
    }
}
