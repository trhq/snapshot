<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotoIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('photos', function (Blueprint $table) {
          $table->foreign('shoot_id')->references('id')->on('shoots')->onDelete('cascade');
          $table->foreign('camera_id')->references('id')->on('cameras');
          $table->foreign('lens_id')->references('id')->on('lens');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('photos', function (Blueprint $table) {
            $table->dropForeign('photos_shoot_id_foreign');
            $table->dropForeign('photos_camera_id_foreign');
            $table->dropForeign('photos_lens_id_foreign');
        });
    }
}
