<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShootIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shoots', function (Blueprint $table) {
          $table->foreign('user_id')->references('id')->on('users');
          $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
          $table->unique('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shoots', function (Blueprint $table) {
            $table->dropForeign('shoots_user_id_foreign');
            $table->dropForeign('shoots_project_id_foreign');
            $table->dropUnique('shoots_name_unique');
        });
    }
}
