<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLensIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lens', function (Blueprint $table) {
          $table->foreign('maker_id')->references('id')->on('makers');
          $table->unique('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lens', function (Blueprint $table) {
          $table->dropForeign('lens_maker_id_foreign');
          $table->dropUnique('lens_name_unique');
        });
    }
}
