<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReverseClientCommentDBConstraint extends Migration
{
  public function up()
  {
      Schema::table('comments', function (Blueprint $table) {
          //
          $table->dropForeign('comments_commentable_id_foreign');
      });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::table('comments', function (Blueprint $table) {
          //
          $table->foreign('commentable_id')->references('id')->on('clients')->onDelete('cascade');
      });
  }
}
