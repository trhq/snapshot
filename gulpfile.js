var elixir = require('laravel-elixir');
var gulp = require('gulp');

require('laravel-elixir-stylus');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss', 'resources/assets/css/sass.css');
    mix.less('app.less', 'resources/assets/css/less.css');
    mix.stylus('app.styl', 'resources/assets/css/stylus.css');
    mix.styles(["sass.css", "less.css", "stylus.css"], 'public/css/app.css', 'resources/assets/css' );
});
