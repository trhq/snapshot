# Snapshot #

Snapshot is a web application for professional photographers. It is designed to be run on a Raspberry Pi which, when attached to a DSLR camera, transmits captured photographs across a network.

When complete it will facilitate numerous wireless applications including:

* Live Previewing

* Photo Streaming

* Reviewing, picking and dropping of photos

* Proof creation

Development on this project has been on hold while I have been completing my studies. 

## TODO

* Research Blade Extensions for a potential solution to [Issue 2: Methods and View Partials](https://bitbucket.org/TeamRadHQ/snapshot/issues/2/methods-and-view-partials-for-common)

* Address outstanding issues.

* Begin mapping out Artisan Commands for conversion.

* Review current data models against issues and research.