@extends('layouts.master', ["pageTitle" => "Add Client", "subTitle" => "New Client"])

@section('content')
  {!! Form::model('client', array(
    'route' => 'client.store',
    'class' => 'form',
    'id' => 'clientForm'
  ))!!}
  @include('client.partials.form' )

  {!! InputGroup::withContents(
    Form::text( 'projectName', Request::get('projectName'), ['placeholder' => 'Name your client\'s first project...', 'id' => 'projectName'] ) )->prepend( '<label for="projectName" title="Project Name">'.Icon::folder_open().'</label>'
  )  !!}
  @include('partials.formError', array('error' => $errors->first('projectName')))

  {!! InputGroup::withContents(
    Form::text( 'shootName', Request::get('shootName'), ['placeholder' => 'Name your client\'s first shoot...', 'id' => 'shootName'] ) )->prepend( '<label for="shootName" title="Shoot Name">'.Icon::camera().'</label>'
  )  !!}
  @include('partials.formError', array('error' => $errors->first('shootName')))


  {!! Button::primary('Create Client!')->submit()->block()!!}
  {!! Form::close() !!}
@endsection

@section('scripts')
  <script>
    $clientForm = $("#clientForm");
    $projectName = $clientForm.children().find('#projectName');
    $shootName = $clientForm.children().find('#shootName');
    
    $shootName.attr('disabled', true);
    $projectName.on('keypress', function() {
      if ($projectName.val()) {
        $shootName.attr('disabled', false);
      } else {
        $shootName.attr('disabled', true);
      }
    });

    console.log($shootName);
  </script>
@endsection
