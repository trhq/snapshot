{{--*/
  $subTitle = $client->name;
  if ($client->name && $client->company) {
    $subTitle.= " - ";
  }
  $subTitle.= $client->company;
/*--}}

@extends('layouts.master', ["pageTitle" => "Edit Client", "subTitle" => $subTitle])

@section('content')
  {!! Form::model($client, array(
    'method'=> 'put',
    'route' => ['client.update', $client->slug],
    'class' => 'form',
    'id' => 'clientForm'
  )) !!}

  {{ Session::get('message') }}
  @include('client.partials.form')
  {!! Button::primary('Update Client')->submit()->block()!!}
  {!! Form::close() !!}
@endsection
