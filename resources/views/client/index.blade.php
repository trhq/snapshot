@extends('layouts.master', ['pageTitle' => 'Clients', 'subTitle' => 'All Clients'])

@section('content')

  @include('client.partials.index-table', array('clients' => $clients, 'cols' => 4))

@endsection
