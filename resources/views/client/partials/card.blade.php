@extends('partials.card', ['model' => $model, 'cols' => $cols])

@section('title')
    @include('partials.field.client-name', array('client' => $model)) <br>

	@if($model->company)

	  @include('partials.field.client-company', array('client' => $model))

	@endif
@overwrite

@section('card') 
	
<tr>
<td>
@include('partials.field.client-email', array('client' => $model)) <br>
</td>
</tr>
<tr>
<td>
@include('partials.field.client-phone', array('client' => $model))
</td>
</tr>
@overwrite