  {{--  Client Name --}}
  {!! InputGroup::withContents(
    Form::text( 'name', Request::get('name'), ['placeholder' => 'Name...', 'id' => 'name', 'class' => 'col-xs-6'] ) )->prepend( '<label for="name">'.Icon::user().'</label>'
  )  !!}
  @include('partials.formError', array('error' => $errors->first('name')))

  {{--  Client Company --}}
  {!! InputGroup::withContents(
    Form::text( 'company', Request::get('company'), ['placeholder' => 'Company...', 'id' => 'company']  ) )->prepend(  '<label for="company">'.Icon::briefcase() .'</label>'
  )  !!}
  @include('partials.formError', array('error' => $errors->first('company')))

  {{--  Client Email --}}
  {!! InputGroup::withContents(
    Form::text( 'email', Request::get('email'), ['placeholder' => 'Email...', 'id' => 'email']  ) )->prepend(  '<label for="email">'.Icon::send() .'</label>'
  )  !!}
  @include('partials.formError', array('error' => $errors->first('email')))

  {{--  Client Phone--}}
  {!! InputGroup::withContents(
    Form::text( 'phone', Request::get('phone'), ['placeholder' => 'Phone...', 'id' => 'phone']  ) )->prepend(  '<label for="phone">'.Icon::phone_alt() .'</label>'
  )  !!}
  @include('partials.formError', array('error' => $errors->first('phone')))

  {{-- Set the Comments Variable To Request--}}
  {{--*/ $comment = Request::get('comment') /*--}}
  {{-- Get stored comments from the database if there are none posted. --}}
  @if (isset($client))
    @if (isset($client->comments->comment) && !Request::get('comment'))
      {{--*/ $comment = $client->comments->comment /*--}}
    @endif
  @endif

  {{--  Client Comments --}}
  {!! InputGroup::withContents(
    Form::textarea( 'comment', $comment, ['placeholder' => 'Client notes...', 'id' => 'comment']   ) )->prepend(  '<label for="notes">'.Icon::comment() .'</label>'
  )  !!}
