<section class="list">

	@foreach($clients as $model)

	    @include('client.partials.card', ['model' => $model, 'cols' => $cols])

	@endforeach

	@include('partials.card-new', ['model' => $model, 'cols' => $cols])

</section>
  

