<section class=" col-md-4 sidebar">

  @include('partials.sidebar.client', array('client' => $client))

  @include('partials.sidebar.summary', array('model' => $client))

  @include('partials.sidebar.contact', array('model' => $client))

  @include('partials.sidebar.comment', array('model' => $client))
    
</section>

