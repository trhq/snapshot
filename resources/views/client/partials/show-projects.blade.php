<section class="index col-md-8" style="padding:0;">
  <h3>Projects</h3>
      @if( $client->projects->count() )
        @include('project.partials.index-table', ['projects' => $client->projects, 'cols' => 6])
      @else
        <p> There are no projects for this client. </p>
        <p>
        	{{ HTML::link(URL::route('project.create') . '?client_id=' . $client->id, 'Add a new project now.', ["title" => "Add a new project"])}}
	  	</p>
      @endif
</section>
