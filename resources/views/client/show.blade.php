
@extends('layouts.master', ["pageTitle" => "Client", "subTitle" => $client->displayName()])

@section('content')
  <section class="actions">
  {!! $client->btn_edit() !!}
  {!! $client->btn_delete()!!}
  {!! Button::withIcon(Icon::folder_open())
    ->asLinkTo(URL::route('project.create') . "?client_id=$client->id")
    ->withAttributes(["title" => "Edit $client->name"])

  !!}
  {!! Button::withIcon(Icon::camera())
    ->asLinkTo(URL::route('shoot.create') . "?client_id=$client->id")
    ->withAttributes(["title" => "Edit $client->name"])

  !!}


  </section>

  @include('client.partials.show-projects', array('client' => $client, 'cols' => 6))

  @include('client.partials.show-contact', array('client' => $client))

@endsection
