<?php
$docTitle = strip_tags($subTitle);
?>

<!DOCTYPE html>
<html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>{!! $docTitle !!}</title>

      <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

      <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
      <link href="https://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,600,700" rel="stylesheet" type="text/css">
      {!! HTML::style('/css/app.css')!!}
      {!! HTML::script('/js/jquery-1.11.3.js')!!}
      {!! HTML::script('/js/bootstrap.js')!!}
    </head>
    <body>
      <header class="container main">
        <h1>{{$pageTitle}}</h1>
      </header>


      <section id="main">
        <section class="container">
          @if(Session::get('message'))
          <p>
            {{ Session::get('message') }}
          </p>
          @endif

          @if( isset($subTitle))
            <h2>{!! $subTitle !!}</h2>
          @endif

          @yield('content')
        </section>
      </section>
    <footer class="container main">
      <p>
         &copy; 2016 Team Rad HQ
      </p>
    </footer>

    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
     <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
     <script>
     $(function() {
       $dp = $( "#datepicker" );
       $dp.datepicker({
         changeMonth: true,
         changeYear:true,
         dateFormat: 'DD dd MM, yy'
       });
     });
     </script>

    @yield('scripts')
    @include('partials.nav')
    </body>
</html>
