
@extends('partials.card', ['model' => $model, 'cols' => $cols])

@section('title')
	@include('partials.icons.' . $model->getRoute())
	Add a new {{ $model->getRoute() }}.
@overwrite

@section('button')
      {!! Button::success(Icon::plus())
      	->asLinkTo(URL::route($model->getRoute().'.create'))->withAttributes(["title" => "Add a new " . $model->getRoute()]) !!}
@overwrite

@section('card')
@overwrite