
<div class="card col-md-{{ $cols }}"  style="min-height:5em;">

	<div class="panel panel-default">
	
	  <div class="panel-heading " style="min-height:5em; padding-left:0.5em; font-size: 1.2em">
		
			<span style="float:right">
			@yield('button', $model->btn_view() )
			</span>	
			
			<span style="padding:0; margin:0"> @yield('title') </span>

		</div>

		<table class="table table-striped table-hover">

	    	@yield('card')

		</table>

	</div>

</div>