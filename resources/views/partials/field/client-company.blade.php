@if($client->company)

<a href="{!! URL::route('client.show', ['slug' => $client->slug]) !!}">
	@include('partials.icons.company')
</a>
{!! HTML::link(\URL::route('client.show', ['slug' => $client->slug]), $client->company, ['title' => "View " . $client->displayName() ]);!!}
@endif