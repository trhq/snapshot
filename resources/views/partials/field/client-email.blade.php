<a href="mailto:{{ $client->email }}" title="Email {{$client->name}}">
	@include('partials.icons.email')
  {{ $client->email }}
</a>
