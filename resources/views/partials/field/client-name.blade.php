

<a href="{!! URL::route('client.show', ['slug' => $client->slug]) !!}">
	@include('partials.icons.client')
</a>
{!! HTML::link(\URL::route('client.show', ['slug' => $client->slug]), $client->name, ['title' => "View " . $client->displayName() ]);!!}


