@include('partials.icons.date')

@if(isset($project->deadline) && $project->deadline->year > 0)
	<em> Due: @date($project->deadline)</em>
@else
	No date set.
@endif