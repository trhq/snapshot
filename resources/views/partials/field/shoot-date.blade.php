@include('partials.icons.date')
@if(isset($shoot->date) && $shoot->date->year > 0)
	<em> Date: @date($shoot->date)</em>
@else
	No date set.
@endif