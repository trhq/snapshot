<?php
// Set the client id
$client_id = Request::input('client_id');
// Set client_id to stored value if it exists and there is no form data.
if ( isset($shoot->id) && !$client_id) {
  $client_id = $shoot->project->client_id;
}
?>

{!! 
	InputGroup::withContents(
		Form::select( 'client_id', \Snapshot\Client::clientList(1), $client_id, ["id" => 'client_id'] )
	)->prepend( '<label for="client_id" title="Client">'.Icon::user().'</label>' )
!!}
