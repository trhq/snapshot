<?php
// Set the project id
$project_id = Request::input('project_id');
// Set project_id to stored value if it exists and there is no form data.
if ( isset($shoot->id) && !$project_id) {
  $project_id = $shoot->project_id;
}
?>

{!! 
	InputGroup::withContents(
		Form::select('project_id', \Snapshot\Project::projectList(), $project_id, ["id" => 'project_id'] )
	)->prepend( '<label for="project_id" title="Project">'.Icon::folder_open().'</label>')
!!}
