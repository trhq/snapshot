@if($error)
  <p class="alert alert-danger">
    {!! Icon::exclamation_sign() !!}
    {{ $error }}
  </p>
@endif
