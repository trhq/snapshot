{!!
  Navbar::withAttributes(["class" => "main"])
    ->withBrand('Snapshot', '/')
    ->withContent(Navigation::links([
      ["link" => URL::route('client.index'), "title" => "Clients"],
      ["link" => URL::route('project.index'), "title" => "Projects"],
      ["link" => URL::route('shoot.index'), "title" => "Shoots"],
      ["link" => URL::route('photo.index'), "title" => "Photos"],
    ])
  )
  !!}
