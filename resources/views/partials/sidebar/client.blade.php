@extends('partials.sidebar.template')

  @section('title')
    Client
  @overwrite

  @section('rows')

      <tr>
        <td>
          @include('partials.field.client-name', array('client' => $client))
        </td>
      </tr>

      <tr>
        <td>
          @include('partials.field.client-company', array('client' => $client))
        </td>
      </tr>

  @overwrite
