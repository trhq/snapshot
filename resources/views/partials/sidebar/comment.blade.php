@extends('partials.sidebar.template')

@section('title')
  Notes
@overwrite

@section('rows')
  
  <tr>
    <td>
      @if ($model->comments)
        {{ $model->comments->comment }}
      @else
        No notes added.
      @endif
    </td>
  </tr>

@overwrite
