@extends('partials.sidebar.template')

@section('title')
  Client
@overwrite

@section('rows')

  <tr>
    <td>
      @include('partials.field.client-email', array('client' => $client))
    </td>
  </tr>

  <tr>
    <td>
      @include('partials.field.client-phone', array('client' => $client))
    </td>
  </tr>

@overwrite
