@extends('partials.sidebar.template')

@section('title')
  Summary
@overwrite

@section('rows')
  
@if(isset($model->deadline))
  <tr>
    <td>
      @include('partials.field.project-deadline', array('project' => $model))
    </td>
  </tr>
@endif

@if(isset($model->location))
  <tr>
    <td>
      @include('partials.field.shoot-location', array('shoot' => $model))
    </td>
  </tr>
@endif

@if(isset($model->date))
  <tr>
    <td>
      @include('partials.field.shoot-date', array('shoot' => $model))
    </td>
  </tr>
@endif

@if(isset($model->projects))
  <tr>
    <td>
      @include('partials.field.project-count', array('model' => $model))
    </td>
  </tr>
@endif

@if($model->getRoute() == 'project' || $model->getRoute() == 'client')
  <tr>
    <td>
      @include('partials.field.shoot-count', array('model' => $model))
    </td>
  </tr>
@endif

  <tr>
    <td>
      @include('partials.field.photo-count', array('model' => $model))
    </td>
  </tr>

@overwrite
