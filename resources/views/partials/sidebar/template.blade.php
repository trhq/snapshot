<div class="panel panel-default">

  <div class="panel-heading">

    <strong> @yield('title') </strong>

  </div>

  <table class="table table-striped table-hover">

    @yield('rows')

  </table>

</div>