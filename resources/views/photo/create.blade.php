@extends('layouts.master', ["pageTitle" => "Add Client", "subTitle" => "New Client"])

@section('content')
  {!! Form::open(array(
    'route' => 'client.store',
    'class' => 'form'
  ))!!}
  @include('client.partials.form' )

  {!! Button::primary('Create Client!')->submit()->block()!!}
  {!! Form::close() !!}
@endsection
