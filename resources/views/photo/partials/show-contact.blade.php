<section class="client-contact table-responsive col-md-4">
  <table class="table table-striped table-hover">
    <thead>
      <tr>
        <th class="col-xs-2">
          <strong>Contact</strong>
        </th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
          @include('client.partials.email', array('client' => $client))
        </td>
      </tr>
      <tr>
        <td>
          @include('client.partials.phone', array('client' => $client))
        </td>
      </tr>
      <tr>
      </tr>
    </tbody>
    <thead>
      <tr>
        <th class="col-xs-2">
          <strong>Notes</strong>
        </th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
          @if ($client->comments)
            {{ $client->comments->comment }}
          @else
            No client notes.
          @endif
        </td>
      </tr>
      <tr>
      </tr>
    </tbody>
  </table>
</section>
