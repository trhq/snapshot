<section class="client-projects table-responsive col-md-8">
  <h3>Projects</h3>
  @if ($client->projects->count() > 0)
    {!!$client->projects_accordion()!!}
  @else
    <p> You haven't added any projects for this client yet. </p>
  @endif
</section>
