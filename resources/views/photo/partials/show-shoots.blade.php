<section class="project-shoots table-responsive">
  <table class="table table-striped table-bordered">
    <thead>
      <tr>
        <th class="col-xs-10"><strong>Shoot Name</strong></th>
        <th class="col-xs-2"></th>
      </tr>
    </thead>
    <tbody>
      @foreach ( $shoots as $shoot )
      <tr>
        <td>
          {{ $shoot->name }}
          <br>

          {{ $shoot->photos->count() }}
          @if ($shoot->photos->count() ==1 )
            photo
          @else
           photos
          @endif
        </td>
        <td>
            {!! Button::withIcon(Icon::search())->asLinkTo('#')!!}
            {!! Button::withIcon(Icon::pencil())->asLinkTo('#')!!}
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>

</section>
