{{--*/
  $subTitle = $client->name;
  if ($client->name && $client->company) {
    $subTitle.= " - ";
  }
  $subTitle.= $client->company;
/*--}}

@extends('layouts.master', ["pageTitle" => "Client", "subTitle" => $subTitle])

@section('content')
  <section class="actions">
{{-- Create the delete button --}}
    {{--*/
    $delBtn = Form::open(array('route' => array('client.destroy', $client->id), 'method' => 'delete'));
    $delBtn.= Button::danger(Icon::ok() . " Delete" )->large()->withAttributes(["class" => "col-xs-5 col-xs-push-2"])->submit();
    $delBtn.= Form::close();
    /*--}}


  {!! Button::withIcon(Icon::pencil())->large()->asLinkTo(URL::route('client.edit', array("id" => $client->slug)))->withAttributes(["title" => "Edit $client->name"]) !!}
  {!! Modal::named('button-1')
        ->withTitle(Icon::exclamation_sign() . ' Warning', 'alert-danger')
        ->withBody("
          <p>Are you sure you want to delete $client->name? </p> <p>If you proceed, all projects, shoots and photos for this client will be permanently removed.")
        ->withFooter(
            Button::success(Icon::remove() . " Cancel" )->large()->withAttributes(["data-dismiss" => "modal", "class" => 'col-xs-5']) .
            $delBtn
            )
        ->withButton(Button::danger(Icon::trash())->large()->withAttributes(["title" => "Delete $client->name"])) !!}



  </section>
  @include('client.partials.show-projects', array('client' => $client))
  @include('client.partials.show-contact', array('client' => $client))
@endsection
