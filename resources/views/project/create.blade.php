@extends('layouts.master', ["pageTitle" => "Add Client", "subTitle" => "New Project"])

@section('content')
  {!! Form::open(array(
    'route' => 'project.store',
    'class' => 'form'
  ))!!}
  @include('project.partials.form' )

  {!! Button::primary('Create Project!')->submit()->block()!!}
  {!! Form::close() !!}
@endsection
