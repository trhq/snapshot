@extends('layouts.master', ["pageTitle" => "Edit Project", "subTitle" => $project->name])

@section('content')
  {!! Form::model($project, array(
    'method'=> 'put',
    'route' => ['project.update', $project->slug],
    'class' => 'form',
  )) !!}

  {{ Session::get('message') }}

  @include('project.partials.form')
  {!! Button::primary('Update Project')->submit()->block()!!}
  {!! Form::close() !!}
@endsection
