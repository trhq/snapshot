@extends('layouts.master', ['pageTitle' => 'Projects', 'subTitle' => 'All Projects'])
@section('content')
<section id="selector">
  @include('partials.form.client-selector')
</section>

<section class="project-list">
  @include('project.partials.index-table', ['projects' => $projects, 'cols' => 4])
</section>
@endsection


@section('scripts')

<script>
$selector = $('#selector');
$clientSelector = $selector.find('.form-control');
$clientSelector.on('change', function() {
  console.log($clientSelector.val());
  window.location.replace("project?client_id=" + $clientSelector.val() );
})

</script>

@endsection