

@extends('partials.card', ['model' => $project, 'cols' => $cols])

@section('title')
  @include('partials.field.project-name', array('project' => $project)) <br>
  @include('partials.field.client-name', array('client' => $project->client))
@overwrite

@section('card')   

<tr>
<td>
    @include('partials.field.project-deadline', array('project' => $project)) <br>
</td>
</tr>

<tr>
<td>
  @include('partials.field.shoot-count', array('model' => $project)) <br>
</td>
</tr>

<tr>
<td>
  @include('partials.field.photo-count', array('model' => $project)) <br>
</td>
</tr>

@overwrite
