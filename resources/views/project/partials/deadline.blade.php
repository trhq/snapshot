  @if ($project->deadline->year !== -1)
    <p> {!! Icon::calendar() !!} <em> Due {{ $project->deadline->format('j/m/Y') }}</em> </p>
  @endif