
  {!! InputGroup::withContents(
    Form::select(
      'client_id', \Snapshot\Client::clientList(1), Request::input('client_id'))
  )->prepend( '<label for="client_id" title="Client">'.Icon::user().'</label>')!!}

  @include('partials.formError', array('error' => $errors->first('client_id')))

  {{--  Project Name --}}
  {!! InputGroup::withContents(
    Form::text( 'name', Request::get('name'), ['placeholder' => 'Name...', 'id' => 'name'] ) )->prepend( '<label for="name" title="Project Name">'.Icon::folder_open().'</label>'
  )  !!}
  @include('partials.formError', array('error' => $errors->first('name')))

  {{--  Project Deadline --}}
  {!! InputGroup::withContents(
    Form::text( 'deadline', Request::get('deadline'), ['placeholder' => 'Deadline...', 'id' => 'datepicker']  ) )->prepend(  '<label for="deadline">'.Icon::calendar() .'</label>'
  )  !!}
  @include('partials.formError', array('error' => $errors->first('phone')))

  {{-- Set the Comments Variable To Request--}}
  {{--*/
    $comment = Request::get('comment');
  /*--}}

  {{-- Get stored comments from the database if there are none posted. --}}
  @if (isset($project))
    @if (isset($project->comments->comment) && !Request::get('comment'))
      {{--*/ $comment = $project->comments->comment /*--}}
    @endif
  @endif

  {{--  Project Comments --}}
  {!! InputGroup::withContents(
    Form::textarea( 'comment', $comment, ['placeholder' => 'Project notes...', 'id' => 'comment']   ) )->prepend(  '<label for="notes">'.Icon::comment() .'</label>'
  )  !!}
