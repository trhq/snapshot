<section class="project list">

	@foreach($projects as $project)

		@include('project.partials.card', ['project' => $project, 'cols' => $cols])

	@endforeach

	@include('partials.card-new', ['model' => $project, 'cols' => $cols])

</section>