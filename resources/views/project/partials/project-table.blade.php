<section class=" col-md-4 sidebar">

  @include('partials.sidebar.client', ['client' => $project->client])

  @include('partials.sidebar.summary', ['model' => $project])
  
  @include('partials.sidebar.comment', ['model' => $project])

</section>