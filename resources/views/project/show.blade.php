@extends('layouts.master', ["pageTitle" => "Project", "subTitle" => $project->name])

@section('content')
  <section class="actions">
    {!! $project->btn_edit()!!}
    {!! $project->btn_delete()!!}
  </section>

<section class="col-md-8 container index">
      <h3> Shoots </h3>

      @if( $project->shoots->count() )
        @include('shoot.partials.index-table', ['shoots' => $project->shoots, 'cols' => 6])
      @else
        <p> There are no shoots for this project. </p>
        <p> {!! HTML::link(\URL::route('shoot.create') . "?project_id=$project->id&client_id=" . $project->client->id, 'Add a shoot to this project.') !!} </p>
      @endif
  </section>

  @include('project.partials.project-table', $project)

@endsection
