@extends('layouts.master', ["pageTitle" => "Add Shoot", "subTitle" => "New Shoot"])

@section('content')
  {!! Form::open(array(
    'route' => 'shoot.store',
    'class' => 'form'
  ))!!}
  @include('shoot.partials.form' )

  {!! Button::primary('Add Shoot!')->submit()->block()!!}
  {!! Form::close() !!}
@endsection
