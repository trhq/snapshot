@extends('layouts.master', ["pageTitle" => "Edit Shoot", "subTitle" => $shoot->name])

@section('content')
  {!! Form::model($shoot, array(
    'method'=> 'put',
    'route' => ['shoot.update', $shoot->slug],
    'class' => 'form',
  )) !!}

  {{ Session::get('message') }}
  @include('shoot.partials.form')
  {!! Button::primary('Update this shoot')->submit()->block()!!}
  {!! Form::close() !!}
@endsection
