<?php
$subTitle = "All Shoots";
if ($client) {
  $subTitle .= " for " . $client->name;
  if ($client->company) $subTitle .= " <small>$client->company</small>";
}
?>

@extends('layouts.master', ['pageTitle' => 'Shoots', 'subTitle' => $subTitle])

@section('content')

<section id="selector">
  @include('partials.form.client-selector')
  @include('partials.form.project-selector')
</section>


@include('shoot.partials.index-table', array('shoot' => $shoots, 'cols' => 4))

@endsection

@section('scripts')
<script>
  $selector = $('#selector');

  $clientSelector = $selector.find('#client_id');

  $clientSelector.on('change', function() {
    console.log($clientSelector.val());
    window.location.replace("shoot?client_id=" + $clientSelector.val() );
  })

  $projectSelector = $selector.find('#project_id');
  
  $projectSelector.on('change', function() {
    console.log($projectSelector.val());
    window.location.replace("shoot?project_id=" + $projectSelector.val() );
  })
</script>
@endsection