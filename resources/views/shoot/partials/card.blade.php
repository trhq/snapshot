@extends('partials.card', ['model' => $shoot, 'cols' => $cols])

@section('title')
  @include('partials.field.shoot-name', array('shoot' => $shoot)) <br>	
  @include('partials.field.client-name', array('client' => $shoot->project->client))
@overwrite

@section('card')   

<tr>
	<td>
	    @include('partials.field.project-name', array('project' => $shoot->project)) <br>
	</td>
</tr>

<tr>
	<td>
	    @include('partials.field.shoot-location', array('project' => $shoot)) <br>
	</td>
</tr>

<tr>
	<td>
		@include('partials.field.shoot-date', array('shoot' => $shoot)) <br>
	</td>
</tr>

<tr>
<td>
  @include('partials.field.photo-count', array('model' => $shoot)) <br>
</td>
</tr>

@overwrite
