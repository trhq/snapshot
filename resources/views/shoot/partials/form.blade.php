  <?php
    // Set the client id
    $client_id = Request::input('client_id');
    // Set client_id to stored value if it exists and there is no form data.
    if ( isset($shoot->id) && !$client_id) {
      $client_id = $shoot->project->client_id;
    }
  ?>

  {!! InputGroup::withContents(
    Form::select(
      'client_id', \Snapshot\Client::clientList(1), $client_id)
  )->prepend( '<label for="client_id" title="Client">'.Icon::user().'</label>')!!}

  <?php
    // Set the project id
    $project_id = Request::input('project_id');
    // Set project_id to stored value if it exists and there is no form data.
    if ( isset($shoot->id) && !$project_id) {
      $project_id = $shoot->project_id;
    }
  ?>

  {!! InputGroup::withContents(
    Form::select(
      'project_id', \Snapshot\Project::projectList(), $project_id)
  )->prepend( '<label for="project_id" title="Project">'.Icon::folder_open().'</label>')!!}

  @include('partials.formError', array('error' => $errors->first('project_id')))

  {{--  Shoot Name --}}
  {!! InputGroup::withContents(
    Form::text( 'name', Request::get('name'), ['placeholder' => 'Name...', 'id' => 'name'] ) )->prepend( '<label for="name">'.Icon::camera().'</label>'
  )  !!}
  @include('partials.formError', array('error' => $errors->first('name')))

  {{--  Shoot Location --}}
  {!! InputGroup::withContents(
    Form::text( 'location', Request::get('location'), ['placeholder' => 'Shoot location...', 'id' => 'location']  ) )->prepend(  '<label for="location">'.Icon::map_marker() .'</label>'
  )  !!}
  @include('partials.formError', array('error' => $errors->first('location')))

{{--  Shoot Date --}}
  {!! InputGroup::withContents(
    Form::text( 'date', Request::get('date'), ['placeholder' => 'Shoot date...', 'id' => 'datepicker']  ) )->prepend(  '<label for="date">'.Icon::calendar() .'</label>'
  )  !!}

  {{-- Set the Comments Variable To Request--}}
  {{--*/ $comment = Request::get('comment') /*--}}
  {{-- Get stored comments from the database if there are none posted. --}}
  @if (isset($shoot))
    @if (isset($shoot->comments->comment) && !Request::get('comment'))
      {{--*/ $comment = $shoot->comments->comment /*--}}
    @endif
  @endif

  {{--  Shoot Comments --}}
  {!! InputGroup::withContents(
    Form::textarea( 'comment', $comment, ['placeholder' => 'Shoot notes...', 'id' => 'comment']   ) )->prepend(  '<label for="notes">'.Icon::comment() .'</label>'
  )  !!}
