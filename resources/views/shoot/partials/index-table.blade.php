<?php
if ( ! isset($cols)) $cols = 4;
?>

<section class="list ">

  @foreach($shoots as $shoot)

  	@include('shoot.partials.card', ['shoot' => $shoot, 'cols' => $cols])
  
  @endforeach

  @include('partials.card-new', ['model' => $shoot, 'cols' => $cols])
  
</section>
