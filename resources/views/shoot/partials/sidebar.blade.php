<section class=" col-md-4 sidebar">

  @include('partials.sidebar.client', ['client' => $shoot->clients->first()])

  @include('partials.sidebar.summary', ['model' => $shoot])
  
  @include('partials.sidebar.comment', ['model' => $shoot])

</section>