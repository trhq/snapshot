
@extends('layouts.master', [
	"pageTitle" => "Project: " . $shoot->project->name, 
	"subTitle" => $shoot->name
])

@section('content')

	<section class="actions">
		{!! $shoot->btn_edit() !!}
		{!! $shoot->btn_delete() !!}
	</section>

	<section class="content col-md-8 index">
		<h3> Photos </h3>
		<p> Add handling for photos here. </p>

	</section>
  @include('shoot.partials.sidebar', $shoot)


@endsection
