@extends('layouts.master', ["pageTitle" => "Welcome", "subTitle" => "Snapshot"])

@section('content')
  <ul>
    <li>
      {{ HTML::link(URL::route('client.index'), 'View Clients', ["title" => "View Clients"])}}
    </li>
    <li>
      {{ HTML::link(URL::route('project.index'), 'View Projects', ["title" => "View Projects"])}}
    </li>
    <li>
      {{ HTML::link(URL::route('shoot.index'), 'View Shoots', ["title" => "View Shoots"])}}
    </li>
    <li>
      {{ HTML::link(URL::route('photo.index'), 'View Photos', ["title" => "View Photos"])}}
    </li>
  </ul>
@endsection
